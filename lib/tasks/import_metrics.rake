namespace :metrics do
  desc "import metrics from CSV file to database"
  task import_csv: :environment do |task|
    csv_file = ENV['file']
    model_name = ENV['model']
    lead_time = ENV['lead_time']

    Rails.logger.tagged("#{ task.name }") do |logger|
      if !csv_file || !model_name || !lead_time
        logger.error("CSV path, lead_time or model name not given")
        exit
      end

      model = Model.find_by(name: model_name)
      unless model
        logger.error("Model not found: #{ model_name }.")
        exit
      end

      import_result = ImportMetricsFromCsv.new(csv_file, model.id, lead_time).call

      import_result[:errors].each do |error_message|
        logger.error(error_message)
      end
    end
  end
end
