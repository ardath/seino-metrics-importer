require 'csv'    

class ImportMetricsFromCsv
  DEFAULT_COLUMN_SEPARATOR = ';'.freeze

  def initialize(csv_file, model_id, lead_time, options={})
    @csv_file = csv_file
    @model_id = model_id
    @lead_time_minutes = lead_time
    @column_separator = options[:column_separator] || DEFAULT_COLUMN_SEPARATOR
    @errors = []
  end

  def call
    CSV.foreach(csv_file, :headers => true, col_sep: column_separator) do |row|
      begin
        Metric.create!(metrics_data(row.to_h))
      rescue  => err
        @errors << err.message
      end
    end

    return { errors: errors }
  end

  private

  attr_reader :csv_file, :column_separator, :model_id, :lead_time_minutes, :errors

  def metrics_data(metrics_params)
    {
      model_id: model_id,
      date: metrics_params["Date"],
      lead_time_minutes: lead_time_minutes,
      "rmse_-1.00-1.00" => metrics_params["RMSE_-1.00_-1.00"],
      nrmse: metrics_params["N-RMSE_-1.00_-1.00"],
      armse9: metrics_params["A-RMSE_9_-1.00_-1.00"],
      "MEAN(O)_-1.00_-1.00": metrics_params["MEAN(O)_-1.00_-1.00"],
      mean_f: metrics_params["MEAN(F)_-1.00_-1.00"],
      bias: metrics_params["BIAS_-1.00_-1.00"],
      r: metrics_params["R_-1.00_-1.00"],
      fss: metrics_params["FSS_9_0.00_0.00"],
      ioa: metrics_params["IOA_-1.00_-1.00"],
      ns: metrics_params["NS_-1.00_-1.00"],
      a: metrics_params["A"],
      b: metrics_params["B"],
      c: metrics_params["C"],
      d: metrics_params["D_0.00_0.00"],
      pod: metrics_params["POD"],
      dpod: metrics_params["DPOD"],
      far: metrics_params["FAR"],
      csi: metrics_params["CSI"],
      sr: metrics_params["SR"],
      pc: metrics_params["PC"],
      tss: metrics_params["TSS"]
    }
  end
end
