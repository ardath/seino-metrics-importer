# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180211230825) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "metrics", force: :cascade do |t|
    t.datetime "date", null: false
    t.decimal "rmse_-1.00-1.00", null: false
    t.decimal "nrmse", null: false
    t.decimal "armse9", null: false
    t.decimal "MEAN(O)_-1.00_-1.00", null: false
    t.decimal "mean_f", null: false
    t.decimal "bias", null: false
    t.decimal "r", null: false
    t.decimal "fss", null: false
    t.decimal "ioa", null: false
    t.decimal "ns", null: false
    t.decimal "a", null: false
    t.decimal "b", null: false
    t.decimal "c", null: false
    t.decimal "d", null: false
    t.decimal "pod", null: false
    t.decimal "dpod", null: false
    t.decimal "far", null: false
    t.decimal "csi", null: false
    t.decimal "sr", null: false
    t.decimal "pc", null: false
    t.decimal "tss", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "lead_time_minutes", null: false
    t.bigint "model_id"
    t.index ["model_id"], name: "index_metrics_on_model_id"
  end

  create_table "models", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "metrics", "models"
end
