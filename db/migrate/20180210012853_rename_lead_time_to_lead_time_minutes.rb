class RenameLeadTimeToLeadTimeMinutes < ActiveRecord::Migration[5.1]
  def change
    rename_column :inca_metrics, :lead_time, :lead_time_minutes
  end
end
