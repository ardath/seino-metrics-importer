class RenameRmseAgain < ActiveRecord::Migration[5.1]
  def change
    rename_column :metrics, :rmse_range_neg1_00__1_00, :"rmse_-1.00-1.00"
  end
end
