class AddLeadTimeToIncaMetrics < ActiveRecord::Migration[5.1]
  def change
    add_column :inca_metrics, :lead_time, :integer
  end
end
