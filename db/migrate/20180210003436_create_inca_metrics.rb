class CreateIncaMetrics < ActiveRecord::Migration[5.1]
  def change
    create_table :inca_metrics do |t|
      t.datetime :date
      t.numeric :rmse
      t.numeric :nrmse
      t.numeric :armse9
      t.numeric :mean_o
      t.numeric :mean_f
      t.numeric :bias
      t.numeric :r
      t.numeric :fss
      t.numeric :ioa
      t.numeric :ns
      t.numeric :a
      t.numeric :b
      t.numeric :c
      t.numeric :d
      t.numeric :pod
      t.numeric :dpod
      t.numeric :far
      t.numeric :csi
      t.numeric :sr
      t.numeric :pc
      t.numeric :tss

      t.timestamps
    end
  end
end
