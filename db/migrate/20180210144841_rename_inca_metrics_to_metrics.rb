class RenameIncaMetricsToMetrics < ActiveRecord::Migration[5.1]
  def change
    rename_table :inca_metrics, :metrics
  end
end
