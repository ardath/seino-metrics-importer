class RenameRmse < ActiveRecord::Migration[5.1]
  def change
    rename_column :inca_metrics, :rmse, :rmse_range_neg1_00__1_00
  end
end
