class AddNotNullConstraintToIncaMetrics < ActiveRecord::Migration[5.1]
  def change
    change_column :inca_metrics, :date, :datetime, null: false
    change_column :inca_metrics, :rmse, :decimal, null: false
    change_column :inca_metrics, :nrmse, :decimal, null: false
    change_column :inca_metrics, :armse9, :decimal, null: false
    change_column :inca_metrics, :mean_o, :decimal, null: false
    change_column :inca_metrics, :mean_f, :decimal, null: false
    change_column :inca_metrics, :bias, :decimal, null: false
    change_column :inca_metrics, :r, :decimal, null: false
    change_column :inca_metrics, :fss, :decimal, null: false
    change_column :inca_metrics, :ioa, :decimal, null: false
    change_column :inca_metrics, :ns, :decimal, null: false
    change_column :inca_metrics, :a, :decimal, null: false
    change_column :inca_metrics, :b, :decimal, null: false
    change_column :inca_metrics, :c, :decimal, null: false
    change_column :inca_metrics, :d, :decimal, null: false
    change_column :inca_metrics, :pod, :decimal, null: false
    change_column :inca_metrics, :dpod, :decimal, null: false
    change_column :inca_metrics, :far, :decimal, null: false
    change_column :inca_metrics, :csi, :decimal, null: false
    change_column :inca_metrics, :sr, :decimal, null: false
    change_column :inca_metrics, :pc, :decimal, null: false
    change_column :inca_metrics, :tss, :decimal, null: false
    change_column :inca_metrics, :lead_time, :integer, null: false
  end
end
