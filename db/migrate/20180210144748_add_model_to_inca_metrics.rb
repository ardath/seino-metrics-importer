class AddModelToIncaMetrics < ActiveRecord::Migration[5.1]
  def change
    add_reference :inca_metrics, :model, foreign_key: true
  end
end
