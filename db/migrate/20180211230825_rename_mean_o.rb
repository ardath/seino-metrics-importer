class RenameMeanO < ActiveRecord::Migration[5.1]
  def change
    rename_column :metrics, :mean_o, :"MEAN(O)_-1.00_-1.00"
  end
end
